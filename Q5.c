#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *args[]){
    if (argc != 3){
        printf("ERROR!! INVALID ARGUMENTS! Usage: Q5 <source file path> <dest file directory>\n");
        exit(1);
    }
    else{
        FILE *src, *des;
        char command[2000];
        char source[100];
        snprintf(source, sizeof(source),"%s",args[1]);
        char dest[100];
        snprintf(dest, sizeof(dest),"%s",args[2]);
        snprintf(command, sizeof(command), "cp %s %s",source,dest);
        src = fopen(source, "r");
        des = fopen(dest, "w");
        int status1 = fclose(src);
        int status2 = fclose(des);
        if(status1 ==0 && status2 == 0){
            system(command);
            printf("\nDONE.\n");
        }
        else
            printf("ERROR!!\n");
    }
    return 0;
}
