#include<iostream>
#include<queue>
#include<stdlib.h>
#include<algorithm>
#include<vector>

using namespace std;

int pclock = 0;
class roundRobin;
class Process{
    int arrivalTime, burstTime, turnAroundTime, waitTime, completionTime, id, remainingTime;
    bool scheduled;
    friend class roundRobin;
public:
    Process(){
        this->arrivalTime = 0;
        this->burstTime = 0;
        this->scheduled = false;
        id = 0;
        this->remainingTime = 0;
    }
    Process(int arrivalTime, int burstTime, int id){
        this->arrivalTime = arrivalTime;
        this->burstTime = burstTime;
        this->scheduled = false;
        this->id = id;
        this->remainingTime = burstTime;
    }
    void operator=(const Process &p){
        this->arrivalTime = p.arrivalTime;
        this->burstTime = p.burstTime;
        this->completionTime = p.completionTime;
        this->remainingTime = p.remainingTime;
        this->waitTime = p.waitTime;
        this->turnAroundTime = p.turnAroundTime;
        this->id = p.id;
        this->scheduled = p.scheduled;
    }
    bool isArrived(){
        bool status;
        if(this->arrivalTime <= pclock)
            status = true;
        return status;
    }
};

class roundRobin{
    queue <Process> readyQueue;
    queue <Process> jobQueue;
    int timeQuantum, numOfProcess;
    float avgTAT, avgWT;
public:
    roundRobin(){
        int at,bt;
        cout<<"Enter the process details:-\n";
        cout<<"Enter the number of Processes: ";
        cin>>numOfProcess;
        for(int i = 0; i < numOfProcess; i++){
            cout<<"Enter the Arrival Time for P"<<i<<": ";
            cin>>at;
            cout<<"Enter the Burst Time for P"<<i<<": ";
            cin>>bt;
            jobQueue.push(Process(at, bt, i));
        }
        cout<<"ENTER THE TIME QUANTUM: ";
        cin>>timeQuantum;
    }
    void schedule(){
        Process current;
        int count = 1;
        cout<<"\n//////*****Scheduling*****//////\n";
        cout<<"0\n";
        while(!jobQueue.empty()){
            readyQueue.push(jobQueue.front());
            jobQueue.pop();
        }
        while(!readyQueue.empty() && !readyQueue.front().scheduled){
            current = readyQueue.front();
            readyQueue.pop();
            if(current.isArrived())
                cout<<"\tP"<<current.id<<endl;
            if(current.remainingTime > timeQuantum && current.isArrived()){
                current.remainingTime -= timeQuantum;
                pclock += timeQuantum;
                cout<<pclock<<endl;
                readyQueue.push(current);                
            }
            else if(current.remainingTime <= timeQuantum && current.isArrived()){
                pclock += current.remainingTime;
                current.remainingTime = 0;
                current.scheduled = true;
                current.completionTime = pclock;
                current.turnAroundTime = current.completionTime - current.arrivalTime;
                current.waitTime = current.turnAroundTime - current.burstTime;
                jobQueue.push(current);
                cout<<pclock<<endl;
            }
            else if(count == numOfProcess){
                pclock++;
                readyQueue.push(current);
                count = 0;
            }
            else{
                count++;
                readyQueue.push(current);
            }
        }
        calcAvgTAT();
        calcAvgWT();
        show();
    }
    void show(){
        Process current;
        cout<<"\n//////*****DONE!*****//////\n";
        for(int i = 0; i < numOfProcess; i++){
            current = jobQueue.front();
            jobQueue.pop();
            cout<<"P"<<current.id<<": \n";
            cout<<"ARRIVAL TIME: "<<current.arrivalTime
                <<"\tCOMPLETION TIME: "<<current.completionTime
                <<"\tBURST TIME: "<<current.burstTime
                <<"\tTAT: "<<current.turnAroundTime
                <<"\tWT: "<<current.waitTime
                <<endl;
        }
        cout<<"\nAVERAGE TURN AROUND TIME: "<<avgTAT<<endl;
        cout<<"\nAVERAGE WAIT TIME: "<<avgWT<<endl<<endl;
    }
    void calcAvgTAT(){
        Process current;
        float count = numOfProcess;
        float sum = 0.0;
        for(int i = 0; i < numOfProcess; i++){
            current = jobQueue.front();
            jobQueue.pop();
            sum += current.turnAroundTime;
            jobQueue.push(current);
        }
        this->avgTAT = (sum/count);
    }
    void calcAvgWT(){
        Process current;
        float count = numOfProcess;
        float sum = 0.0;
        for(int i = 0; i < numOfProcess; i++){
            current = jobQueue.front();
            jobQueue.pop();
            sum += current.waitTime;
            jobQueue.push(current);
        }
        this->avgWT = (sum/count);
    }
};

int main(){
    system("clear");
    roundRobin scheduler;
    scheduler.schedule();
    return 0;
}
