#include<iostream>
#include<queue>
#include<algorithm>
#include<pthread.h>

using namespace std;

int pclock = 0;
class SJF;
class Process{
    int arrivalTime, burstTime, turnAroundTime, waitTime, completionTime, id, remainingTime, actualArrival;
    bool scheduled;
    friend class SJF;
public:
    Process(){
        this->arrivalTime = 0;
        this->burstTime = 0;
        this->scheduled = false;
        id = 0;
        this->remainingTime = 0;
    }
    Process(int arrivalTime, int burstTime, int id){
        this->arrivalTime = arrivalTime;
        this->burstTime = burstTime;
        this->scheduled = false;
        this->id = id;
        this->remainingTime = burstTime;
    }
    void operator=(const Process &p){
        this->arrivalTime = p.arrivalTime;
        this->burstTime = p.burstTime;
        this->completionTime = p.completionTime;
        this->remainingTime = p.remainingTime;
        this->waitTime = p.waitTime;
        this->turnAroundTime = p.turnAroundTime;
        this->id = p.id;
        this->scheduled = p.scheduled;
    }
    bool operator<(const Process &p){
        bool status = false;
        if (this->burstTime < p.burstTime)
            status = true;
        return status;
    }
    bool isArrived(){
        bool status;
        if(this->arrivalTime <= pclock)
            status = true;
        return status;
    }
};

class SJF{
    queue <Process> jobQueue;
    queue <Process> readyQueue;
    int numOfProcess;
    vector <int> bts;
    float avgTAT, avgWT;
public:
    SJF(){
        int at, bt;
        cout<<"Enter the number of processes: ";
        cin>>numOfProcess;
        for(int i = 0; i < numOfProcess; i++){
            cout<<"Enter Arrival Time for P"<<i<<": ";
            cin>>at;
            cout<<"Enter Burst Time for P"<<i<<": ";
            cin>>bt;
            bts.push_back(bt);
            jobQueue.push(Process(at,bt,i));
        }
        sort(&jobQueue.front(), &jobQueue.back());
    }
    void scheduler(){
        Process current;
        for(int i = 0; i < numOfProcess; i++){
            readyQueue.push(jobQueue.front());
            jobQueue.pop();
        }
        cout<<"\n######*****SCHEDULING*****######\n";
        cout<<"\n0"<<endl;
        while(!readyQueue.empty() && !readyQueue.front().scheduled){
            current = readyQueue.front();
            if(current.isArrived() && !current.scheduled){
                cout<<"\tP"<<current.id<<endl;
                if(current.remainingTime > readyQueue.front().remainingTime && readyQueue.front().isArrived())
                    current = contextSwitch(current);
                current.actualArrival = pclock;
                pclock += current.burstTime;
                cout<<pclock<<endl;
                current.completionTime = pclock;
                current.turnAroundTime = current.completionTime - current.arrivalTime;
                current.waitTime = current.turnAroundTime - current.burstTime;
                current.scheduled = true;
                jobQueue.push(current);
                readyQueue.pop();
            }
            else{
                readyQueue.push(current);
                pclock++;
            }
        }
        calcAvgTAT();
        calcAvgWT();
        show();
    }
    Process contextSwitch(Process process){
        process.actualArrival = pclock;
        process.remainingTime = pclock - process.actualArrival;
        Process temp = process;
        readyQueue.push(temp);
        process = readyQueue.front();
        return process;
    }
    void show(){
        Process current;
        cout<<"\n//////*****DONE!*****//////\n";
        for(int i = 0; i < numOfProcess; i++){
            current = jobQueue.front();
            jobQueue.pop();
            cout<<"P"<<current.id<<": \n";
            cout<<"ARRIVAL TIME: "<<current.arrivalTime
                <<"\tCOMPLETION TIME: "<<current.completionTime
                <<"\tBURST TIME: "<<current.burstTime
                <<"\tTAT: "<<current.turnAroundTime
                <<"\tWT: "<<current.waitTime
                <<endl;
        }
        cout<<"\nAVERAGE TURN AROUND TIME: "<<avgTAT<<endl;
        cout<<"\nAVERAGE WAIT TIME: "<<avgWT<<endl<<endl;
    }
    void calcAvgTAT(){
        Process current;
        float count = numOfProcess;
        float sum = 0.0;
        for(int i = 0; i < numOfProcess; i++){
            current = jobQueue.front();
            jobQueue.pop();
            sum += current.turnAroundTime;
            jobQueue.push(current);
        }
        this->avgTAT = (sum/count);
    }
    void calcAvgWT(){
        Process current;
        float count = numOfProcess;
        float sum = 0.0;
        for(int i = 0; i < numOfProcess; i++){
            current = jobQueue.front();
            jobQueue.pop();
            sum += current.waitTime;
            jobQueue.push(current);
        }
        this->avgWT = (sum/count);
    }
};

int main(){
    //system("clear");
    SJF scheduling;
    scheduling.scheduler();
    return 0;
}
