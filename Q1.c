#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<sys/types.h>

void sameProgram_sameCode(){
    printf("Now parent and child will execute the same code and print their PIDs and a sum of digits in number 69\n");
    fork();
    printf("PID is : %d\n",getpid());
    printf("Sum of digits in 69 is: %d\n",(6+9));
    exit(1);
}

void sameProgram_diffCode(){
    printf("Now, parent and child will execeute different code, print their PIDs.\n");
    pid_t pid = fork();
    if(pid == 0){
        printf("In CHILD process with PID: %d\n", getpid());
        exit(1);
    }
    else if(pid>0){
        printf("In PARENT process with PID: %d\n", getpid());
        exit(1);
    }
    else{
        printf("ERROR IN FORKING! DUDE, your system allright?!\n");
        exit(1);
    }
}

void parentWaitsForChild(){
    printf("Now, parent and child will execeute different code, parent will wait for child to finish.\n");
    pid_t pid = fork();
    if(pid == 0){
        printf("In CHILD process with PID: %d\n", getpid());
        sleep(5);
        printf("DONE with this child!\n");
        exit(1);
    }
    else if(pid>0){
        printf("In PARENT process with PID: %d\n", getpid());
        while(!wait(NULL))
            printf(".");
        printf("CHILD returned!! \n");
        exit(1);
    }
    else{
        printf("ERROR IN FORKING! DUDE, your system allright?!\n");
        exit(1);
    }
}

int main(){
    int choice;
    do{
        printf("1. Same Program, Same Code.\n2. Same Program, Different Code.\n3. Parent Program waits for the Child to finish.\n0. EXIT\n");
        scanf("%d",&choice);
        switch(choice){
            case 1:{
                sameProgram_sameCode();
            }
            break;
                case 2:{
                sameProgram_diffCode();
            }
            break;
            case 3:{
                parentWaitsForChild();
            }
            break;
            case 0:{
                exit(1);
            }
            break;
            default:{
                printf("ERROR! ENTER A CORRECT CHOICE!! Are you alright?\n");
            }
        }
    }while(choice!=0);
}
