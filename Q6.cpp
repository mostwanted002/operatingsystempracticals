#include<iostream>
#include<stdlib.h>

using namespace std;

int pclock = 0;
class FCFS;
class Process{
    int arrivalTime, burstTime, turnAroundTime, waitTime, completionTime, id;
    bool scheduled;
    friend class FCFS;
public:
    Process(){
        this->arrivalTime = 0;
        this->burstTime = 0;
        this->scheduled = false;
        id = 0;
    }
    Process(int arrivalTime, int burstTime, int id){
        this->arrivalTime = arrivalTime;
        this->burstTime = burstTime;
        this->scheduled = false;
        this->id = id;
    }
    Process(const Process &p){
        this->arrivalTime = p.arrivalTime;
        this->burstTime = p.burstTime;
    }
};

class FCFS{
    int numOfProcess;
    float avgTAT, avgWT;
    Process *queue;

public:
    FCFS(){
        int at,bt;
        cout<<"Enter the number of processes: ";
        cin>>numOfProcess;
        queue = new Process[numOfProcess];
        for(int i = 0; i < numOfProcess; i++){
            cout<<"Enter the Arrival Time for P"<<i<<": ";
            cin>>at;
            cout<<"Enter the Burst Time for P"<<i<<": ";
            cin>>bt;
            queue[at] = Process(at, bt, i);
        }
    }
    void scheduler(){
        cout<<"\n//////*****Scheduling*****//////\n";
        cout<<"0"<<endl;
        for(int i = 0; i < numOfProcess; i++){
            if(!queue[i].scheduled){
            cout<<"\tP"<<queue[i].id<<endl;
            pclock += queue[i].burstTime;
            queue[i].completionTime = pclock;
            queue[i].turnAroundTime = queue[i].completionTime - queue[i].arrivalTime;
            queue[i].waitTime = queue[i].turnAroundTime - queue[i].burstTime;
            queue[i].scheduled = true;
            cout<<pclock<<endl;
            }
        }
        calcAvgTAT();
        calcAvgWT();
        show();
    }
    void calcAvgTAT(){
        float count = numOfProcess;
        float sum = 0.0;
        for(int i = 0; i < numOfProcess; i++)
            sum += queue[i].turnAroundTime;
        this->avgTAT = (sum/count);
    }
    void calcAvgWT(){
        float count = numOfProcess;
        float sum = 0.0;
        for(int i = 0; i < numOfProcess; i++)
            sum += queue[i].waitTime;
        this->avgWT = (sum/count);
    }
    void show(){
        cout<<"\n//////*****DONE!*****//////\n";
        for(int i = 0; i < numOfProcess; i++){
            cout<<"P"<<i<<": \n";
            cout<<"ARRIVAL TIME: "<<queue[i].arrivalTime
                <<"\tCOMPLETION TIME: "<<queue[i].completionTime
                <<"\tBURST TIME: "<<queue[i].burstTime
                <<"\tTAT: "<<queue[i].turnAroundTime
                <<"\tWT: "<<queue[i].waitTime
                <<endl;
        }
        cout<<"\nAVERAGE TURN AROUND TIME: "<<avgTAT<<endl;
        cout<<"\nAVERAGE WAIT TIME: "<<avgWT<<endl<<endl;
    }
};

int main(){
    system("clear");
    FCFS schedule;
    schedule.scheduler();
    return 0;
}
